import { filter, add, remove, update } from "../handlers/listHandler.js";

let list = [
  { id: 1, title: "The Alchemist", author: "Paulo Coelho", year: 1988 },
  { id: 2, title: "The Prophet", author: "Kahlil Gibran", year: 1923 },
];

const Books = async (req, res) => {
  req.on("data", (data) => {
    let body = JSON.parse(data.toString());
    switch (req.method) {
      case "GET":
        res.end(JSON.stringify(list));
        break;
      case "POST":
        list = add(list, body);
        res.end(JSON.stringify(list));
        break;
      case "PUT":
        list = update(list, body);
        res.end(JSON.stringify(list));
        break;
      case "DELETE":
        list = remove(list, body.id);
        res.end(JSON.stringify(list));
        break;
      default:
        res.statusCode = 404;
        res.end(JSON.stringify({ error: "Resource not found" }));
        break;
    }
  });
};

export default Books;
