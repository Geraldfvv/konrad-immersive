export const filter = (list, id) => {};

export const add = (list, newElement) => {
  newElement.id = list.length + 1;
  return list.concat(newElement);
};

export const remove = (list, id) => {
  return list.filter((element) => element.id !== id);
};

export const update = (list, updatedElement) => {
  list.map((element) => {
    if (element.id == updatedElement.id) {
      Object.keys(element).forEach((key) => {
        element[key] = updatedElement[key];
      });
    }
  });
  return list;
};
