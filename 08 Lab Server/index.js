import http from "http";

import Books from "./routes/books.js";
import Authors from "./routes/authors.js";

const port = process.env.PORT || 3000;
const host = "localhost";

const requestListener = function (req, res) {
  res.setHeader("Content-Type", "application/json");
  res.setHeader("Access-Control-Allow-Origin", ["*"]);
  res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type");
  res.statusCode = 200;

  switch (req.url) {
    case req.url.match(/\/books\/[0-9]*/)?.input:
      Books(req, res);
      break;
    case req.url.match(/\/authors\/[0-9]*/)?.input:
      Authors(req, res);
      break;

    default:
      res.statusCode = 404;
      res.end(JSON.stringify({ error: "Resource not found" }));
      return;
  }
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});
