const body = document.body;
import mock from "./mock.json";
import "./styles.scss";

let url =
  "https://api.nasa.gov/planetary/apod?api_key=82Iq9iexsl3BUfRNAWgXiCn9NKJxZWfC4eaj6sjn";
let promise = fetch(url);

promise
  .then((x) => result.json())
  .then((data) => {
    console.log(data);
    addElements(data.title, data.explanation, data.url);
  })
  .catch((error) => {
    console.log(error)
  });

function addElements(dataTitle, dataDescription, url) {
  const container = document.createElement("div");
  container.setAttribute("id", "container");

  const img = document.createElement("img");
  img.setAttribute("id", "img");
  img.src = url;

  const title = document.createElement("h1");
  title.setAttribute("id", "title");
  title.innerHTML = dataTitle;

  const description = document.createElement("p");
  description.setAttribute("id", "description");
  description.innerHTML = dataDescription;

  description.addEventListener("mouseover", function (event) {
    if (description.classList.contains("hide")) {
      description.classList.remove("hide");
      description.classList.add("show");
    } else {
      description.classList.remove("show");
      description.classList.add("hide");
    }
  });

  container.appendChild(img);
  container.appendChild(title);
  container.appendChild(description);
  body.appendChild(container);
}
