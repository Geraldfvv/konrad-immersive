errorHandler = (err, req, res, next) => {
  console.log("ERROR ", JSON.parse({ errors: err.message }));
  res.json(JSON.parse({ errors: err.message }));
};

module.exports = errorHandler;
