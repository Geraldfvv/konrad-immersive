userValidate = (req, res, next) => {
  let errors = {};
  const genderEnum = ["male", "female", "other"];
  const cardTypeEnum = ["visa", "mastercard", "other"];
  const data = req.body;

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
  const cardRegex =
    /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;
  const dateRegex =
    /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

  // First name required
  if (!data.firstName) errors.firstName = "First name is required!";

  // Last name required
  if (!data.lastName) errors.lastName = "Last name is required!";

  // Birthday required and date format
  if (!data.birthday) errors.birthday = "Birthday is required!";
  else if (!dateRegex.test(data.birthday))
    errors.birthday = "Birthday format invalid";

  // Gender required and enum option
  if (!data.gender) errors.gender = "Gender is required!";
  else if (!genderEnum.includes(data.gender))
    errors.gender = "Invalid gender option";

  // Username required
  if (!data.username) errors.username = "Username is required!";
  else if (data.username.length < 5)
    errors.username = "Username must contain at least 5 characters";

  // Email required and email format
  if (!data.email) errors.email = "Email is required!";
  else if (!emailRegex.test(data.email))
    errors.email = "This is not a valid email format!";

  //Password required and min 8 char
  if (!data.password) errors.password = "Password is required!";
  else if (data.password.length < 8)
    errors.password = "Password must contain at least 8 characters";

  //Password confirmation required and same as password
  if (!data.passwordVerification)
    errors.passwordVerification = "Password confirmation is required!";
  else if (data.passwordVerification !== data.password)
    errors.passwordVerification = "Passwords do not match";

  // Card type required and card enum
  if (!data.cardType) errors.cardType = "Card Type is required!";
  else if (!cardTypeEnum.includes(data.cardType))
    errors.cardType = "Invalid card type option";

  // Card number required and card number format
  if (!data.cardNumber) errors.cardNumber = "Card Number is required!";
  else if (!cardRegex.test(data.cardNumber))
    errors.cardNumber = "This is not a valid card number format!";

  // Card expiration date required and date format
  if (!data.cardExpiration)
    errors.cardExpiration = "Card expiration date is required!";
  else if (!dateRegex.test(data.cardExpiration))
    errors.cardExpiration = "Card expiration date format invalid";

  // Card CVS required and format
  if (!data.cardCVS) errors.cardCVS = "Card CVS is required!";

  if (Object.keys(errors).length === 0) {
    next();
  } else {
    throw Error(errors);
  }
};

module.exports = userValidate;
