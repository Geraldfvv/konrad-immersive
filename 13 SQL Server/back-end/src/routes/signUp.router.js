const express = require("express");
const pool = require("../config/database.config");
const userValidate = require("../middleware/userValidate.middleware");
const signUpRouter = express.Router();

signUpRouter.route("/").post(userValidate, async (req, res, next) => {
  const {
    firstName,
    lastName,
    birthday,
    gender,
    username,
    email,
    password,
    passwordVerification,
    cardType,
    cardNumber,
    cardExpiration,
    cardCVS,
  } = req.body;

  try {
    pool.query(
      "INSERT INTO users (firstName, lastName, birthday, gender, username, email, password, passwordVerification, cardType, cardNumber, cardExpiration, cardCVS) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING *",
      [
        firstName,
        lastName,
        birthday,
        gender,
        username,
        email,
        password,
        passwordVerification,
        cardType,
        cardNumber,
        cardExpiration,
        cardCVS,
      ],
      (error, results) => {
        if (error) {
          res.statusCode = 500
          res.json({ errors: error.detail });
        } else {
          res.json(results.rows[0]);
        }
      }
    );
  } catch (error) {
    throw Error(error);
  }
});

module.exports = signUpRouter;
