const express = require("express");
const cors = require("cors");
const signUpRouter = require("./routes/signUp.router.js");

const logger = require("./middleware/logger.middleware.js");
const errorHandler = require("./middleware/error.middleware.js");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(logger);

app.use("/signup", signUpRouter);
app.use(errorHandler);

module.exports = app;
