import { v4 as uuidv4 } from "uuid";

export const authors = [
  {
    id: "asasa15sd",
    name: "Hans Christian Andersen",
    birth: "1835",
    country: "DK",
    books: ["Book 1"],
  },
  {
    id: "a61d2s",
    name: "Honoré de Balzac",
    birth: "1835",
    country: "FR",
    books: ["Book 1", "Book 2"],
  },
  {
    id: "asd15",
    name: "Paul Celan",
    birth: "1952",
    country: "RM, FR",
    books: ["Book 1", "Book 2", "Book 3"],
  },
];

export function getAuthor(id) {
  return authors.find((author) => author.id === id);
}
