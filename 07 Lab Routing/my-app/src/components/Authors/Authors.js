import { Link, Outlet } from "react-router-dom";
import { authors } from "../../data/data";
import "./Authors.scss";

export const Authors = () => {
  const block = "author-list";

  return (
    <>
      <div className={`${block}__root`}>
        {authors.map((author) => (
          <Link
            to={`/authors/${author.id}`}
            key={author.id}
            className="author-list__link"
          >
            {author.name}
          </Link>
        ))}
      </div>

      <Outlet />
    </>
  );
};
