import { useParams } from "react-router-dom";
import {getAuthor} from "../../data/data";

export const Author = () => {

  let {authorId} = useParams();
  let {name,birth,country} = getAuthor(authorId);

  
  return (
    <>
      <h1>Author: {name}</h1>
      <h2>Birth Year: {birth}</h2>
      <h3>country: {country}</h3>
    </>
  )
};
