const express = require("express");
let list = require("../../seeds/authors.js");
const authorRouter = express.Router();

authorRouter.use((req, res, next) => {
  console.log("Time: ", Date.now(), " Method: ", req.method);
  next();
});

authorRouter.get("/", (req, res) => {
  res.send(JSON.stringify(list));
});

authorRouter.get("/:id", (req, res) => {
  list.map((element) => {
    if (element.id.toString() == req.params.id) {
      res.send(JSON.stringify(element));
    }
  });
});

authorRouter.post("/", (req, res) => {
  console.log(req.body)
  req.body.id = list.length + 1;
  list.push(req.body);
  res.json(req.body);
});

authorRouter.put("/", (req, res) => {
  list.map((element) => {
    if (element.id == req.body.id) {
      Object.keys(element).forEach((key) => {
        element[key] = req.body[key];
      });
    }
  });
  res.json(req.body);
});

authorRouter.delete("/", (req, res) => {
  list = list.filter((element) => element.id !== req.body.id);
  res.json(req.body);
});

module.exports = authorRouter;
