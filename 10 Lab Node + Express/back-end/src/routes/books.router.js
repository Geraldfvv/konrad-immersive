let list = require("../../seeds/book.js");
const bookRouter = express.Router();

bookRouter.use((req, res, next) => {
  console.log("Time: ", Date.now(), " Method: ", req.method);
  next();
});

bookRouter.get("/", (req, res) => {
  res.send(JSON.stringify(list));
});

bookRouter.get("/:id", (req, res) => {
  list.map((element) => {
    console.log(element.id.toString(), req.params.id);
    if (element.id.toString() == req.params.id) {
      res.send(JSON.stringify(element));
    }
  });
});

bookRouter.post("/", (req, res) => {
  req.body.id = list.length + 1;
  list.push(req.body);
  res.send(req.body);
});

bookRouter.put("/", (req, res) => {
  list.map((element) => {
    if (element.id == req.body.id) {
      Object.keys(element).forEach((key) => {
        element[key] = req.body[key];
      });
    }
  });
  res.send(req.body);
});

bookRouter.delete("/", (req, res) => {
  list = list.filter((element) => element.id !== req.body.id);
  res.send(req.body);
});

module.exports = bookRouter;
