const express = require("express");
const authorRouter = require("./routes/authors.router.js");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.header("Content-Type", "application/json");
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Methods",
    "GET,POST,PUT,PATCH,DELETE,HEAD,OPTIONS"
  );
  res.header(
    "Access-Control-Request-Headers",
    "Content-Type, Origin, Accept,Authorization,Content-Length, X-Requested-With"
  );
  next();
});

app.use("/authors", authorRouter);


module.exports = app;
