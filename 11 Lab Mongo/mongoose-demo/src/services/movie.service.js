const Movie = require("../models/movie.model");

class MovieService {
  // Query all documents
  static async getAllMovies() {
    const movies = await Movie.find();

    return movies;
  }
  // Query single document
  static async getMovieById(id) {
    const movie = await Movie.findById(id);

    return movie;
  }
  // Add new document
  static async addMovie(movieData) {
    const movie = new Movie(movieData);

    await movie.save();

    return movie;
  }
  // Update document by ID
  static async updateMovie(id, newData) {
    const updatedMovie = await Movie.findByIdAndUpdate(id, newData, {
      returnDocument: "after",
      runValidators: true,
    });

    return updatedMovie;
  }
  // Delete document by ID
  static async deleteMovie(id) {
    const movie = await Movie.findByIdAndDelete(id);

    return movie;
  }
}

module.exports = MovieService;
