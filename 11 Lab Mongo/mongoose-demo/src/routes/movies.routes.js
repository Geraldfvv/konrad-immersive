const express = require("express");
const MovieService = require("../services/movie.service");
const moviesRouter = express.Router();
const {
  formatRequestError,
  isValidationError,
} = require("../helpers/errors.helper");

moviesRouter
  .route("/")
  // Get all movies
  // Endpoint: http://localhost:3000/movies
  .get(async (req, res) => {
    const movies = await MovieService.getAllMovies();

    res.json(movies);
  })
  // Create new movie
  // Endpoint: http://localhost:3000/movies
  .post(async (req, res) => {
    const movieData = req.body;

    try {
      const newMovie = await MovieService.addMovie(movieData);

      res.send(newMovie);
    } catch (err) {
      res
        .status(isValidationError(err) ? 400 : 500)
        .send(formatRequestError(err));
    }
  });

moviesRouter
  .route("/:id")
  // Get single movie
  // Endpoint: http://localhost:3000/movies/movieID
  .get(async (req, res) => {
    const movieID = req.params.id;
    const movie = await MovieService.getMovieById(movieID);

    !movie ? res.sendStatus(404) : res.json(movie);
  })
  // Update movie by giving id
  // Endpoint: http://localhost:3000/movies/movieID
  .put(async (req, res) => {
    const movieID = req.params.id;
    const movieData = req.body;

    try {
      // find and update a movie, if no document is found, null will be returned
      const updatedMovie = await MovieService.updateMovie(movieID, movieData);

      !updatedMovie ? res.sendStatus(404) : res.json(updatedMovie);
    } catch (err) {
      res
        .status(isValidationError(err) ? 400 : 500)
        .send(formatRequestError(err));
    }
  })
  // Delete movie by giving id
  // Endpoint: http://localhost:3000/movies/movieID
  .delete(async (req, res) => {
    const movieID = req.params.id;
    // find and delete a movie, if no document is found, null will be returned
    const deletedMovie = await MovieService.deleteMovie(movieID);

    !deletedMovie ? res.sendStatus(404) : res.json(deletedMovie);
  });

module.exports = moviesRouter;
