const express = require("express");
const mongoose = require("mongoose");
const moviesRouter = require("./routes/movies.routes");

// MongoDB Variables
const DB_HOST = process.env.DB_HOST || "mongodb://localhost:27017";
const DB_NAME = process.env.DB_NAME || "immersive-movies";

// Create new express application instance
const app = express();

// Parse request body data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Open connection to MongoDB
mongoose
  .connect(`${DB_HOST}/${DB_NAME}`)
  .then(() => {
    console.log(`Connection to ${DB_HOST}/${DB_NAME} opened`);
  })
  .catch((err) => {
    console.log("Error connecting to mongo: ", err);
  });

// Load moviesRouter in the app
app.use("/movies", moviesRouter);

module.exports = app;
