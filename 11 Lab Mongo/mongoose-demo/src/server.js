const app = require("./app");
const PORT = process.env.PORT || 3000;

/**
 * Server function
 *
 * PORT: 3000
 * HOSTNAME: localhost
 */

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
