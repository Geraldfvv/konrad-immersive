// file: movie.model.js

const mongoose = require("mongoose");

// Create new Mongoose Schema
const movieSchema = new mongoose.Schema({
  // Set name as String type and add a minlength validation
  name: { type: String, minlength: 3 },
  // Set rating as Number type
  rating: Number,
  // Set published_at as Date type and make the field required
  published_at: { type: Date, required: true },
});

// Create new Mongoose Model
const Movie = mongoose.model("Movie", movieSchema);

module.exports = Movie;
