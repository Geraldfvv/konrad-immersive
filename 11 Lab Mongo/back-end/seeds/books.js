let list = [
  { id: 1, title: "The Alchemist", author: "Paulo Coelho", year: 1988 },
  { id: 2, title: "The Prophet", author: "Kahlil Gibran", year: 1923 },
];

module.exports = list;