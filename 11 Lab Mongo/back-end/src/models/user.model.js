const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  fullName: { type: String, required: true },
  password: { type: String, minlength: 8, required: true },
  userName: { type: String, required: true, unique: true },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
