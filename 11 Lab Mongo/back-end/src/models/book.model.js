const mongoose = require("mongoose");

const bookSchema = new mongoose.Schema({
  name: { type: String, required: true },
  author: { type: String, minlength: 3 },
  published_at: Date,
});

const Book = mongoose.model("Book", bookSchema);

module.exports = Book;