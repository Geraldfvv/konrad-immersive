const mongoose = require("mongoose");

const authorSchema = new mongoose.Schema({
  fullName: { type: String, required: true },
  birthday: Date,
  imageURL : String
});

const Author = mongoose.model("Author", authorSchema);

module.exports = Author;
