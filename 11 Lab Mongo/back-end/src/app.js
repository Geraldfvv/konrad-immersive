const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");

const cors = require("cors");
const authorRouter = require("./routes/authors.router.js");
const signUpRouter = require("./routes/signUp.router.js");

const logger = require("./middleware/logger.middleware.js");
const authenticateJWT = require("./middleware/jwt.middleware.js");
const errorHandler = require("./middleware/error.middleware.js");
const logInRouter = require("./routes/logIn.router.js");

const DB_HOST = process.env.DB_HOST || "mongodb://localhost:27017";
const DB_NAME = process.env.DB_NAME || "Konrad-Library";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  cors({
    origin: "http://localhost:3001",
    credentials: true,
  })
);

app.use(cookieParser());

app.use(logger);
app.post(["/authors", "/books"], contentValidator);

mongoose
  .connect(`${DB_HOST}/${DB_NAME}`)
  .then(() => {
    console.log(`Connection to ${DB_HOST}/${DB_NAME} opened`);
  })
  .catch((err) => {
    console.log("Error connecting to mongo: ", err);
  });

app.use("/authors", authenticateJWT, authorRouter);
app.use("/signup", signUpRouter);
app.use("/login", logInRouter);

app.use(errorHandler);

module.exports = app;
