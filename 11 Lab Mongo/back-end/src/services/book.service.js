const Book = require("../models/book.model");

class BookService {

  static async getAllBooks() {
    const books = await Book.find();
    return books;
  }

  static async getBookById(id) {
    const book = await Book.findById(id);
    return book;
  }

  static async addBook(newBook) {
    const book = new Book(newBook);
    await book.save();
    return book;
  }

  static async updateBook(id, book) {
    const updatedBook = await Book.findByIdAndUpdate(id, book, {
      returnDocument: "after",
      runValidators: true,
    });
    return updatedBook;
  }

  static async deleteBook(id) {
    const book = await Book.findByIdAndDelete(id);
    return book;
  }
}

module.exports = BookService;
