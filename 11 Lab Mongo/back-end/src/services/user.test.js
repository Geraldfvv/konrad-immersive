const mockingoose = require("mockingoose");

const UserService = require("../services/user.service");
const User = require("../models/user.model");

const MOCK_USERS = [
  {
    _id: "62b23886333ee63cb72b5f8f" ,
    fullName: "Gerald",
    password: "1234567890",
    userName: "geraldfvv",
    __v: 0,
  },
  {
    fullName: "Alberto",
    password: "1234567890",
    userName: "albertoto",
    __v: 0,
    _id: "62b34ddacbb741236954f4f9",
  },
];

describe("User service tests", () => {
  test("Filter user by username", async () => {
    mockingoose(User).toReturn(MOCK_USERS[0], "find");
    const user = await UserService.getUser("geraldfvv");
    expect(user.fullName).toEqual(MOCK_USERS[0].fullName);
  });

  test("Add User", async () => {
    mockingoose(User).toReturn(MOCK_USERS[1], "save");
    const newUser = await UserService.addUser({
      fullName: "Alberto",
      password: "1234567890",
      userName: "albertoto",
    });
    expect(newUser.fullName).toEqual(MOCK_USERS[1].fullName);
  });
});
