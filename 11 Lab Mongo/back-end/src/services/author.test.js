const mockingoose = require("mockingoose");
const mongoose = require("mongoose");

const AuthorService = require("../services/author.service");
const Author = require("../models/author.model");

const MOCK_AUTHOR = [
  {
    _id: "62b11599889918475860051d",
    fullName: "Stephen King",
    birthday: "1947-09-21T06:00:00.000+00:00",
    __v: 0,
    imageURL:
      "https://m.media-amazon.com/images/S/amzn-author-media-prod/fkeglaqq0pic05a0v6ieqt4iv5._SX450_.jpg",
  },
  {
    _id: "62b115e0889918475860051f",
    fullName: "Barbara Cartwda",
    birthday: "1901-09-06T00:00:00.000+00:00",
    __v: 0,
    imageURL: "https://images.gr-assets.com/authors/1363041660p8/10320.jpg",
  },
  {
    _id: "62b36c26c3c0159eb69ebde5",
    fullName: "Paulo Coelho",
    birthday: "2022-06-17T00:00:00.000+00:00",
    imageURL:
      "https://www.planetadelibros.com/usuaris/autores/fotos/5/tam_1/000004270_1_Paulo_Coelho_C_Xavier_Gonzalez_201808221739.jpg",
    __v: 0,
  },
];

describe("Authors service tests", () => {
  test("getAllAuthors", async () => {
    mockingoose(Author).toReturn(MOCK_AUTHOR, "find");
    const authors = await AuthorService.getAllAuthors();
    expect(authors.map((author) => author.fullName)).toEqual([
      "Stephen King",
      "Barbara Cartwda",
      "Paulo Coelho",
    ]);
  });

  test("addAuthor", async () => {
    mockingoose(Author).toReturn(MOCK_AUTHOR[0], "save");
    const newAuthor = await AuthorService.addAuthor({
      fullName: "Stephen King",
      birthday: "1947-09-21T06:00:00.000+00:00",
      imageURL:

        "https://m.media-amazon.com/images/S/amzn-author-media-prod/fkeglaqq0pic05a0v6ieqt4iv5._SX450_.jpg",
    });
    expect(newAuthor.fullName).toEqual(MOCK_AUTHOR[0].fullName);
  });

  test("updateAuthor", async () => {
    mockingoose(Author).toReturn(MOCK_AUTHOR[0], "findOneAndUpdate");
    const editedAuthor = await AuthorService.updateAuthor(
      "62b11599889918475860051d",
      {
        fullName: "Stephen King",
        birthday: "1947-09-21T06:00:00.000+00:00",
        imageURL:
          "https://m.media-amazon.com/images/S/amzn-author-media-prod/fkeglaqq0pic05a0v6ieqt4iv5._SX450_.jpg",
      }
    );
    expect(editedAuthor.fullName).toEqual("Stephen King");
  });

  test("deleteAuthor", async () => {
    mockingoose(Author).toReturn(MOCK_AUTHOR[0], "findOneAndDelete");
    const deletedAuthor = await AuthorService.deleteAuthor(
      "62b11599889918475860051d"
    );
    expect(deletedAuthor.fullName).toEqual("Stephen King");
  });
});
