const Author = require("../models/author.model");

class AuthorService {

  static async getAllAuthors() {
    const authors = await Author.find();
    return authors;
  }

  static async getAuthorById(id) {
    const author = await Author.findById(id);
    return author;
  }

  static async addAuthor(newAuthor) {
    const author = new Author(newAuthor);
    await author.save();
    return author;
  }

  static async updateAuthor(id, author) {
    const updatedAuthor = await Author.findOneAndUpdate ({_id : id}, author, {
      returnDocument: "after",
      returnNewDocument: true,
    });
    return updatedAuthor;
  }

  static async deleteAuthor(id) {
    const author = await Author.findOneAndDelete({_id:id});
    return author;
  }
}

module.exports = AuthorService;
