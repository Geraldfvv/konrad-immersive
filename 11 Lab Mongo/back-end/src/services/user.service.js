const User = require("../models/user.model");

class UserService {
  static async getUser(userName) {
    const user = await User.find({ userName: userName });
    return user;
  }

  static async addUser(newUser) {
    const user = new User(newUser);
    await user.save();
    return user; 
  }
}

module.exports = UserService;
