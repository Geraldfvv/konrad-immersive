const formatRequestError = (err) => ({
  error: err.name,
  message: err.message,
  statusCode: err.status,
});

const errorCode = (err) => {
  switch (err.name) {
    case "ValidationError":
      return 400;
    case "WrongContentType":
      return 400;
    case "ItemNotFound":
      return 404;
    case "CastError":
      return 400;
    case "InvalidCredentials":
      return 501;
    case "UserNotFound":
      return 501;
    default:
      return 500;
  }
};

const throwError = (message, name, code) => {
  let err = new Error(message);
  err.name = name;
  throw err;
};

module.exports = {
  formatRequestError,
  errorCode,
  throwError,
};
