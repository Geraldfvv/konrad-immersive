const app = require("./app.js")

const port = process.env.PORT || 3000;
const host = "localhost";

app.listen(3000, () =>{
  console.log(`Server is running on http://${host}:${port}`);
})



