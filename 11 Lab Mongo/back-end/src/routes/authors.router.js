const express = require("express");
const AuthorService = require("../services/author.service");
const authorsRouter = express.Router();
const { throwError } = require("../helpers/errors.helper");
const contentValidator = require("../middleware/content-type.middleware");

authorsRouter
  .route("/")
  .get(async (req, res) => {
    try {
      const authors = await AuthorService.getAllAuthors();
      res.json({ data: authors, statusCode: 200 });
    } catch (err){
      next(err);
    }
  })

  .post(contentValidator, async (req, res, next) => {
    const authorData = req.body;
    try {
      const newAuthor = await AuthorService.addAuthor(authorData);
      res.send({ data: newAuthor, statusCode: 200 });
    } catch (err) {
      next(err);
    }
  });

authorsRouter
  .route("/:id")
  .get(async (req, res, next) => {
    try {
      const authorID = req.params.id;
      const author = await AuthorService.getAuthorById(authorID);
      !author ? throwError("Item Not Found", "ItemNotFound") : res.json({ data: author, statusCode: 200 });
    } catch (err) {
      next(err);
    }
  })

  .put(async (req, res, next) => {
    const authorID = req.params.id;
    const authorData = req.body;

    try {
      const updatedAuthor = await AuthorService.updateAuthor(
        authorID,
        authorData
      );
      !updatedAuthor
        ? throwError("Item Not Found", "ItemNotFound")
        : res.json({ data: updatedAuthor, statusCode: 200 });
    } catch (err) {
      next(err);
    }
  })

  .delete(async (req, res, next) => {
    try {
      const authorID = req.params.id;
      const deletedAuthor = await AuthorService.deleteAuthor(authorID);
      !deletedAuthor
        ? throwError("Item Not Found", "ItemNotFound")
        : res.json({ data: deletedAuthor, statusCode: 200 });
    } catch (err) {
      next(err);
    }
  });

module.exports = authorsRouter;
