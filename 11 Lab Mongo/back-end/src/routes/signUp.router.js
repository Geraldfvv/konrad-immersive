const express = require("express");
const UserService = require("../services/user.service");
const { jwtSign } = require("../helpers/jwt.helper");
const signUpRouter = express.Router();

signUpRouter.route("/").post(contentValidator, async (req, res, next) => {
  const userData = req.body;
  try {
    const newUser = await UserService.addUser(userData);
    const token = jwtSign(newUser.userName);
    res.json({ token: token , statusCode: 200 });
  } catch (err) {
    next(err);
  }
});

module.exports = signUpRouter;
