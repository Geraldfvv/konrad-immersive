const express = require("express");
const UserService = require("../services/user.service");
const { throwError } = require("../helpers/errors.helper");
const { jwtSign } = require("../helpers/jwt.helper");
const logInRouter = express.Router();

logInRouter.route("/").post(contentValidator, async (req, res, next) => {
  const userData = req.body;
  try {
    const logUser = await UserService.getUser(userData.userName);
    if (logUser.length === 0) {
      throwError("User Not Found", "UserNotFound");
    } else {
      if (logUser[0].password === userData.password) {
        const token = jwtSign(logUser[0].userName);
        res.json({ token: token , statusCode: 200 });
      } else {
        throwError("Invalid Credentials", "InvalidCredentials");
      }
    }
  } catch (err) {
    next(err);
  }
});

module.exports = logInRouter;
