const express = require("express");
const BookService = require("../services/book.service");
const booksRouter = express.Router();
const {
  formatRequestError,
  isValidationError,
} = require("../helpers/errors.helper");

booksRouter
  .route("/")
  .get(async (req, res) => {
    const books = await BookService.getAllBooks();
    res.json(books);
  })

  .post(async (req, res) => {
    const bookData = req.body;
    try {
      const newBook = await BookService.addBook(bookData);
      res.send(newBook);
    } catch (err) {
      res
        .status(isValidationError(err) ? 400 : 500)
        .send(formatRequestError(err));
    }
  });

booksRouter
  .route("/:id")
  .get(async (req, res) => {
    const bookID = req.params.id;
    const book = await BookService.getBookById(bookID);

    !book ? res.sendStatus(404) : res.json(book);
  })

  .put(async (req, res) => {
    const bookID = req.params.id;
    const bookData = req.body;
    
    try {
      const updatedBook = await BookService.updateBook(
        bookID,
        bookData
      );
      !updatedBook ? res.sendStatus(404) : res.json(updatedBook);
    } catch (err) {
      res
        .status(isValidationError(err) ? 400 : 500)
        .send(formatRequestError(err));
    }
  })

  .delete(async (req, res) => {
    const bookID = req.params.id;
    const deletedBook = await BookService.deleteBook(bookID);
    !deletedBook ? res.sendStatus(404) : res.json(deletedBook);
  });

module.exports = booksRouter;
