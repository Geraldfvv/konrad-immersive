const { throwError } = require("../helpers/errors.helper");

contentValidator = (req, res, next) => {
  if (req.header("Content-Type") === "application/json") {
    next();
  } else {
    throwError("Wrong Content Type", "WrongContentType");
  }
};

module.exports = contentValidator;
