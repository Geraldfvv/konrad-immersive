const { errorCode , formatRequestError } = require("../helpers/errors.helper");

errorHandler = (err, req, res, next) => {
  console.log("ERROR ", err.message);
  err.status = errorCode(err);
  res.status(err.status).send(formatRequestError(err));
};

module.exports = errorHandler;
