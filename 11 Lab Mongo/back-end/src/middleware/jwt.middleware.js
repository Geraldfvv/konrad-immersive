const { throwError } = require("../helpers/errors.helper");
const jwt = require("jsonwebtoken");

authenticateJWT = (req, res, next) => {
  const authCookies = req.cookies.token;

  if (authCookies) {
    jwt.verify(authCookies, "my-ultra-secure-key", (err) => {
      if (err) {
        throwError("Unauthorized", "Unauthorized");
      }
      next();
    });
  } else {
    throwError("Unauthorized", "Unauthorized");
  }
};

module.exports = authenticateJWT;
